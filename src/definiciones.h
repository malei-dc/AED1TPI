//
// Created by Pablo on 03/10/2017.
//

#ifndef UTILES_DEFINICIONES_H
#define UTILES_DEFINICIONES_H

#include <iostream>
#include <vector>
#include <tuple>

using namespace std;


// CONSTANTES
enum Item {CODUSU,ANO4,COMPONENTE,NIVEL_ED,ESTADO,CAT_OCUP,EDAD,PP3E_TOT,PP04D_COD,P21,P47T,ITF,IX_TOT,IX_MAYEQ10,CAT_INAC};
// DEFINICIONES DE TIPO
typedef vector<int> individuo;
typedef vector<individuo> eph;
typedef tuple<int,float> paritaria_exitosa;
typedef vector<paritaria_exitosa> lista_exitosos;

#endif //UTILES_DEFINICIONES_H
