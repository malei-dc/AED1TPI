#include "ejercicios.h"


/*-------------------------- PROCEDIMIENTO 1 -------------------------- */
bool esEncuestaValida(eph t){

    return t.size() > 0 && esMatriz(t) && individuosValidos(t) && individuosDistintos(t) && mismoAno(t) &&
           hogaresCoherentes(t);
}

//funciones para ENCUESTA VALIDA

bool esMatriz(eph t){
    bool res=true;
    for (int i = 0; i < t.size(); i++) {
        for (int j = 0; j < t.size(); j++) {
            if (t[i].size() != t[j].size())
                res = false;
        }
    }
    return res;
}

bool individuosValidos(eph t){
    bool res = true;
    for (int i = 0; i < t.size(); ++i) {
        if(!individuoValido(t[i]))
            res = false;
    }
    return res;
}

bool individuoValido(individuo p){
    bool returncito1 = p.size() == 15 && p[CODUSU] > 0 && p[COMPONENTE] > 0 && p[ANO4] > 0 && 0 <= p[NIVEL_ED] && p[NIVEL_ED] <= 1 &&
                       ((1 <= p[ESTADO] && p[ESTADO]  <= 4) || p[ESTADO] == 9 || p[ESTADO] == 99 || p[ESTADO] == 999 ||
                        p[ESTADO] == 9999) &&
                       ((0 <= p[CAT_OCUP] && p[CAT_OCUP] <= 4) || p[CAT_OCUP] == 9 || p[CAT_OCUP] == 99 || p[CAT_OCUP] == 999 ||
                        p[CAT_OCUP] == 9999);

    bool returncito2 = 0 <= p[EDAD] && p[EDAD] <= 110 && -1 <= p[PP3E_TOT] && p[ITF] >= 0 && (p[P21] == (-9) || p[P21] >= 0) &&
                       (p[P47T] == (-9) || p[P47T] == (-1) || p[P47T] >= 0) &&
                       p[PP04D_COD] >= -1 && 0 <= p[CAT_INAC] && p[CAT_INAC] <= 7 && 0 < p[IX_MAYEQ10] && 0 < p[IX_TOT] &&
                       p[IX_MAYEQ10] <= p[IX_TOT] &&
                       noTrabajaNoTienePago(p) && noTrabajaNoTieneHoras(p) &&
                       (p[PP3E_TOT] == -1) == (p[PP04D_COD] == -1);

    return returncito1 && returncito2;
}

bool noTrabajaNoTieneHoras(individuo p){return p[ESTADO]!=1 == (p[PP3E_TOT]==-1);}
bool noTrabajaNoTienePago(individuo p){return !(p[ESTADO]!=1 && p[P21]!=0);}

bool individuosDistintos(eph t){
    bool res = true;
    for (int i = 0; i < t.size(); ++i) {
        for (int j = 0; j < t.size(); ++j) {
            if(i != j && (t[i][CODUSU] == t[j][CODUSU]) && (t[i][COMPONENTE]==t[j][COMPONENTE]))
                res = false;
        }
    }
    return res;
}

bool mismoAno(eph t){
    bool res = true;
    for (int i = 0; i < t.size(); ++i) {
        if (t[i][ANO4] != t[0][ANO4])
            res = false;
    }
    return res;
}

bool hogaresCoherentes (eph t){
    bool res = true;
    for (int i = 0; i < t.size(); ++i) {
        for (int j = 0; j < t.size(); ++j) {
            if (mismoHogar(t[i], t[j]) &&
                !(mismoITF(t[i], t[j]) && mismaCantidadMiembros(t[i], t[j]) && mismaCantidadAdultos(t[i], t[j]))) {
                res = false;
            }
        }
    }
    return res;
}

bool mismoHogar (individuo p1, individuo p2){ return p1[CODUSU] == p2[CODUSU]; }
bool mismoITF (individuo p1, individuo p2){ return p1[ITF] == p2[ITF]; }
bool mismaCantidadMiembros (individuo p1, individuo p2){ return p1[IX_TOT] == p2[IX_TOT]; }
bool mismaCantidadAdultos (individuo p1, individuo p2){ return p1[IX_MAYEQ10] == p2[IX_MAYEQ10]; }


/*-------------------------- PROCEDIMIENTO 2 --------------------------*/
int laMejorEdad(eph t){
    int edad = -1;
    for (int i=0; i<t.size(); i++){
        if (promedioIngresoXEdad(t,t[i][EDAD]) >= promedioIngresoXEdad(t,edad))
                edad = t[i][EDAD];
    }
    return edad;
}

//funciones de LA MEJOR EDAD

int cantidadEdad (eph t, int edad){
    int cantidadEdad = 0;
    for (int i=0; i<t.size(); i++)
        if (t[i][EDAD] == edad && t[i][P47T] >= 0)
            cantidadEdad ++;
    return cantidadEdad;
}

int sumaIngresoXEdad (eph t, int edad){
    int sumaIngresoXEdad = 0;
    for (int i=0; i<t.size(); i++)
        if (t[i][EDAD] == edad && t[i][P47T] >= 0)
            sumaIngresoXEdad += t[i][P47T];
    return sumaIngresoXEdad;

}

int promedioIngresoXEdad (eph t, int edad){
    int promedioIngresoXEdad = 0;
    if(cantidadEdad(t,edad) != 0)
        promedioIngresoXEdad = sumaIngresoXEdad(t,edad)/cantidadEdad(t,edad);
    return promedioIngresoXEdad;
}


/*-------------------------- PROCEDIMIENTO 3 --------------------------*/
float promedioIngresoProfesional(eph t){
    return ingresosPromedioProfXHora(t);
}

//funciones para PROMEDIO INGRESO PROFESIONAL
int ingresosPromedioProfXHora (eph t){
    float sumaIngresosProfesionales = 0;
    int ingresosPromedioProfXHora = 0;
    for (int i=0; i<t.size(); i++)
        if (esProfesionalConIngresos(t[i]))
            sumaIngresosProfesionales = sumaIngresosProfesionales + ( t[i][P21] / (4*t[i][PP3E_TOT]) ) ;
    ingresosPromedioProfXHora = sumaIngresosProfesionales / cantProfesionales(t);
    return ingresosPromedioProfXHora;
}

bool esProfesionalConIngresos (individuo p){
    return (esProfesional(p) && declaroIngresosYHoras(p));
}

int cantProfesionales (eph t){
    int cantProfesionales = 0;
    for (int i=0; i<t.size(); i++)
        if (esProfesionalConIngresos(t[i]))
            cantProfesionales ++;
    return cantProfesionales;
}

bool esProfesional (individuo p){
    return (p[NIVEL_ED]==1 && p[ESTADO]==1);
}
bool declaroIngresosYHoras (individuo p){
    return (p[P21] > 0 && p[PP3E_TOT] > 0);
}



/*-------------------------- PROCEDIMIENTO 4 --------------------------*/
int hogarDeMayorIngreso(eph t){
    int result = -1;
    for (int i=0; i<t.size(); i++)
        if (esHogar(t, t[i][CODUSU]) && esElDeMayorIngreso(t,t[i][CODUSU]) )
            result = t[i][CODUSU];
    return result;
}

// Funciones para HOGAR DE MAYOR INGRESO

bool esHogar (eph t, int c){
    bool esHogar = false;
    for (int i=0; i<t.size(); i++)
        if (t[i][CODUSU] == c)
            esHogar = true;
    return esHogar;
}

bool esMayorIngreso (eph t, int cod, int result){
    return (ingresoHogar(t,result) > ingresoHogar(t,cod));
}

int ingresoHogar (eph t, int cod){
    int ingresoHogar = 0;
    for (int i=0; i<t.size(); i++)
        if (t[i][CODUSU] == cod )
            ingresoHogar = ingresoHogar + t[i][ITF];
    return ingresoHogar;
}

bool igualIngresoMenorIndice (eph t, int cod, int result){
    return (ingresoHogar(t,result) == ingresoHogar(t,cod)) && (indiceCodusu(t,result) <= indiceCodusu(t,cod));
}

int indiceCodusu (eph t, int cod){
    int indiceCodusu = 0;
    for (int i=0; i<t.size(); i++)
        indiceCodusu = indiceCodusu + i;
    return indiceCodusu;
}

bool esElDeMayorIngreso (eph t, int result){
    bool esElDeMayorIngreso = true;
    for (int i=0; i<t.size(); i++) {
        int cod = t[i][CODUSU];
        if (esHogar(t, cod) && !esMayorIngreso(t,cod, result) && !igualIngresoMenorIndice(t,cod,result))
            esElDeMayorIngreso = false;
    }
    return esElDeMayorIngreso;
}


/*-------------------------- PROCEDIMIENTO 5 --------------------------*/
individuo mejorNoProfesional(eph t){
    individuo ind;
    for (int i=0; i<t.size(); i++) {
        if (esNoProfesionalCIMAPP(t, t[i])) {
            ind = t[i];
            break;
        }
    }
    return ind;
}

// Funciones de MEJOR NO PROFESIONAL

bool esNoProfesionalCIMAPP (eph t, individuo p){
    return (p[NIVEL_ED] == 0) && (p[ESTADO] == 1) && (p[PP3E_TOT] > 0) &&
           ((p[P21] / (4 * p[PP3E_TOT])) > ingresosPromedioProfXHora(t));
}


/*-------------------------- PROCEDIMIENTO 6 --------------------------*/
bool sigoEstudiando(eph t){
    return desocupadosNoUniversitarios(t) > desocupadosUniversitarios(t);
}

// Funciones de sigoEstudiando

int desocupadosNoUniversitarios (eph t){
    int desocupadosNoUniversitarios = 0;
    for (int i=0; i<t.size(); i++)
        if (t[i][NIVEL_ED]==0 && t[i][ESTADO]==2)
            desocupadosNoUniversitarios ++;
    return desocupadosNoUniversitarios;
}

int desocupadosUniversitarios (eph t){
    int desocupadosNoUniversitarios = 0;
    for (int i=0; i<t.size(); i++)
        if (t[i][NIVEL_ED]==1 && t[i][ESTADO]==2)
            desocupadosNoUniversitarios ++;
    return desocupadosNoUniversitarios;
}


/*-------------------------- PROCEDIMIENTO 7 --------------------------*/
individuo empleadoDelAnio(eph t){
    individuo ind;
    for (int i=0; i<t.size(); i++)
        if(esEmpleadoDelAno(t,t[i]))
            ind = t[i];
    return ind;
}

// Funciones de empleadoDelAño

bool esEmpleadoDelAno (eph t, individuo p){
    bool esEmpleadoDelAno = false;
    if (p[CAT_OCUP] != 1 && existePatronQueGanaMenos(t,p)) esEmpleadoDelAno = true;
    return esEmpleadoDelAno;
}

bool existePatronQueGanaMenos (eph t, individuo p){
    bool existePatronQueGanaMenos = false;
    for (int i=0; i<t.size(); i++)
        if ( t[i][CAT_OCUP]==1 && t[i][P21]<p[P21] )
            existePatronQueGanaMenos = true;
    return existePatronQueGanaMenos;
}


/*-------------------------- PROCEDIMIENTO 8 --------------------------*/
bool noTieneAQuienCuidar(eph t){

    bool res = false;
    for (int i = 0; i < t.size(); ++i) {
        if(t[i][CAT_OCUP]==4 && (t[i][IX_TOT]-t[i][IX_MAYEQ10])==0)
            res = true;
    }
    return res;
}


/*-------------------------- PROCEDIMIENTO 9 --------------------------*/
bool pareto(eph t){
    bool res = false;
    ordenarPorP47T(t);
    int veintePorciento = 0.2*t.size();
    if(sumaIngresos(t, 0, veintePorciento)> 0.8*sumaIngresos(t,0,t.size()))
        res = true;
    return res;
}

//funciones para PARETO

int sumaIngresos (eph p, int desde, int hasta){
    int suma = 0;
    for (int i=desde; i<hasta; i++){
        suma = suma+p[i][P47T];
    }
    return suma;
}

void ordenarPorP47T(eph &t){
    individuo aux;
    for (int i = 1; i < t.size(); ++i) {
        for (int j = i; j > 0; --j) {
            if(t[j][P47T]>t[j-1][P47T]){
                aux = t[j-1];
                t[j-1]=t[j];
                t[j]=aux;
            }
        }
    }
}


/*-------------------------- PROCEDIMIENTO 10 --------------------------*/
int elDeMayorIncrementoInterAnual(eph t1, eph t2){
    int elDeMayorIncrementoInterAnual = -1;
    for (int j=0; j<t2.size(); j++)
        for (int i=0; i<t1.size(); i++)
            if (esParecido(t1[i],t2[j]) && esElDeMayorIncremento(t1,t2,t1[i],t2[j]))
                elDeMayorIncrementoInterAnual = j;
    return elDeMayorIncrementoInterAnual;
}

// Funciones de elDeMayorIncrementoInterAnual

bool columnasIgual (individuo p1, individuo p2){
    return p1[CODUSU]==p2[CODUSU] && p1[COMPONENTE]==p2[COMPONENTE];
}
bool columnasDistintas (individuo p1, individuo p2){
    return (p1[ANO4]<p2[ANO4]) && ( !(p1[NIVEL_ED]==1) || (p2[NIVEL_ED]!=0) ) && pasanLosAnios(p1,p2);
}

bool esElDeMayorIncremento (eph t1, eph t2, individuo p1, individuo p2){
    bool esElDeMayorIncremento2 = true;
    for (int i = 0; i < t1.size(); i++)
        for (int j = 0; j < t2.size(); j++)
            if (seMantiene(t1,t2,t1[i],t2[j]) && t2[j][P21] - t1[i][P21] > p2[P21] - p1[P21])
                esElDeMayorIncremento2 = false;
    return esElDeMayorIncremento2;
}

bool seMantiene (eph t1, eph t2, individuo p1, individuo p2){
    return esParecido(p1, p2) && mismaActividad(p1, p2) && p1[P21] > 0 && p2[P21] > 0;
}
bool esParecido (individuo p1, individuo p2){ return columnasDistintas(p1,p2) && columnasIgual(p1,p2); }
bool mismaActividad (individuo p1, individuo p2) { return p1[PP04D_COD]==p2[PP04D_COD]; }
bool pasanLosAnios (individuo p1, individuo p2){
    return (p1[EDAD] <= p2[EDAD]) && (p2[EDAD] <= p1[EDAD] + p2[ANO4] - p1[ANO4]);
}


/*-------------------------- PROCEDIMIENTO 11 --------------------------*/
vector<tuple<int,float> > mejorQueLaInflacion(eph t1, eph t2, float infl){
    vector<tuple<int,float> > result;
    tuple <int, float > elemento;

    for (int i = 0; i < t1.size(); ++i) {
        if(t1[i][PP04D_COD]>0  && !estaTupla(result, t1[i][PP04D_COD])&& inflacion(t1, t2, t1[i][PP04D_COD])> infl){//
            get<0>(elemento) = t1[i][PP04D_COD];
            get<1>(elemento) = inflacion(t1, t2, t1[i][PP04D_COD]);
            result.push_back(elemento);
        }
    }
    ordenarPorInflacion(result);
    return result;
}

//FUNCIONES PARA MEJORQUELAINFLACION
float inflacion(eph t1, eph t2, int c){
    float res = 0;
    if(promedioPaga(t1,c)>0)
        res = (promedioPaga(t2,c)/promedioPaga(t1,c))-1;
    return res*100;
}

float promedioPaga(eph t, int c){
    //int res = 0;
    float suma = 0;
    int cantidadCod = cantCodOcup(t,c);
    for (int i = 0; i < t.size(); ++i) {
        if (t[i][PP04D_COD]==c && t[i][P21]>0){
            suma = suma + (t[i][P21]/cantidadCod);
        }
    }
    //res= suma/cantidadCod;
    return suma;
}

int cantCodOcup(eph t, int c){
    int suma = 0;
    for (int i = 0; i < t.size(); ++i) {
        if (t[i][PP04D_COD]==c && t[i][P21]>0)
            suma++;
    }
    return suma;
}

bool estaTupla (vector<tuple <int, float> > t, int c){
    int res = false;
    for (int i = 0; i < t.size(); ++i) {
        if (get<0>(t[i])==c)
            res = true;
    }
    return res;
}

void ordenarPorInflacion(vector<tuple<int,float> > &t){
    tuple<int, float> aux;
    for(int i=1; i<t.size(); i++){
        for(int j=i; j>0; j--){
            if (get<1>(t[j]) >= get<1>(t[j-1])) {
                aux = t[j - 1];
                t[j - 1] = t[j];
                t[j] = aux;
            }
        }
    }
}


/*-------------------------- PROCEDIMIENTO 12 --------------------------*/
void ordenar(eph &t){
    ordenarCODUSU(t);
    ordenarEDADxCODUSU(t);
}

//FUNCIONES PARA ORDENAR POR CODUSU Y EDAD
void ordenarCODUSU(eph &t){
    individuo aux;
    for (int i = 1; i < t.size(); ++i) {
        for (int j = i; j > 0; --j) {
            if(t[j][CODUSU]>t[j-1][CODUSU]){
                aux = t[j-1];
                t[j-1]=t[j];
                t[j]=aux;
            }
        }
    }
}

void ordenarEDADxCODUSU(eph &t) {
    individuo aux;
    for (int i = 1; i < t.size(); ++i) {
        for (int j = i; j > 0; --j) {
            if(t[j][CODUSU] == t[j-1][CODUSU] && t[j][EDAD]>t[j-1][EDAD]){
                aux = t[j-1];
                t[j-1]=t[j];
                t[j]=aux;
            }
        }
    }
}


/*-------------------------- PROCEDIMIENTO 13 --------------------------*/
void agregarOrdenado(eph &t, individuo ind){
    if(!esta(t, ind) && sumaCODUSU(t, ind[CODUSU]) == 0)
        t.push_back(ind);
    ordenar(t);
}

//FUNCIONES PARA AGREGAR ORDENADO
bool esta(eph t, individuo ind){
    bool res = false;
    for (int i = 0; i < t.size(); ++i) {
        if (t[i] == ind)
            res = true;
    }
    return res;
}

/*-------------------------- PROCEDIMIENTO 14 --------------------------*/
void quitar(eph &t, individuo ind){
    if(esta(t, ind) && sumaCODUSU(t, ind[CODUSU]) == 1){
        t= eliminar(t, ind);
    }
}

//FUNCIONES PARA QUITAR
eph eliminar (eph t, individuo ind){
    eph res;
    int i=0;
    while(i<t.size()){
        if(t[i]!= ind){
            res.push_back(t[i]);
            i++;
        } else
            i++;
    }
    return res;
}
int sumaCODUSU(eph t, int n){
    int suma = 0;
    for (int i = 0; i < t.size(); ++i) {
        if (t[i][CODUSU]== n)
            suma++;
    }
    return suma;
}