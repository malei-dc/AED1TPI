#include "../Funciones_TPI.h"
#include "../ejercicios.h"

#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(sigoEstudiando, prueba) {
    eph t1 = leerEncuesta("datos/eth_03.csv");
    EXPECT_EQ(true, sigoEstudiando(t1));
}