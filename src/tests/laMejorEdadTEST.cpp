#include "../Funciones_TPI.h"
#include "../ejercicios.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(laMejorEdad, prueba) {

    eph t1 = {
            {1,  2016, 1, 1, 1, 0, 56, -1, -1,    0,     19400, 19400,  1, 1, 0},
            {2,  2016, 1, 0, 0, 0, 17, -1, -1,    0,     0,     0,      1, 1, 3},
            {4,  2016, 1, 1, 0, 0, 20, -1, -1,    10000, 10000, 10000,  1, 1, 0},
            {3,  2016, 1, 1, 1, 0, 35, 30, 40311, 20000, 20000, 20000,  1, 1, 0},
            {5,  2016, 1, 1, 1, 0, 20, 40, -1,    21000, 21000, 21000,  1, 1, 0},
            {10, 2016, 1, 1, 1, 0, 35, 30, 20301, 20000, 20000, 20000,  1, 1, 0},
    };

    EXPECT_EQ(35, laMejorEdad(t1));
}