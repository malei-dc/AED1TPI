#include "../Funciones_TPI.h"
#include "../ejercicios.h"
#include "gtest/gtest.h"
#include <iostream>
#include <string>

using namespace std;

TEST(mejorNoProfesional, prueba) {
    individuo itest = {7,2016,3,0,1,3,36,30,10313,15000,15000,37800,3,3,0};
    individuo ind;
    eph t1 = leerEncuesta("datos/eth_01.csv");
    ind = mejorNoProfesional(t1);

    EXPECT_EQ(itest, ind);
}